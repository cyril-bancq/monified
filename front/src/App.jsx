
import "../src/style/App.css"
import { Dashboard } from "./components/Dashboard/Dashboard"
import { Sidebar } from "./components/Navigation/Sidebar/Sidebar"

function App() {
  return (
    <div className="App">
      <Sidebar />
      <Dashboard />
    </div>
  )
}

export default App
