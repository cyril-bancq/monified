import React from "react";
import "./Input.css";

export function Input({ placeholder, onChange, type = "text", icon }) {
  return (
    <div className="form-group">
      <span>{icon}</span>
      <input className="form-field" type={type} placeholder={placeholder} />
    </div>
  );
}
