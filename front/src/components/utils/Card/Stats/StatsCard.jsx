import React from "react";
import "./StatsCard.css";

export function StatsCard({ icon, title, value, backcolor, color }) {
  return (
    <div className="stats__container" style={{ backgroundColor: backcolor }}>
      <div className="stats__left" style={{ backgroundColor: color}}>
        <span>{icon}</span>
      </div>
      <div className="stats__right">
        <h1>{title}</h1>
        <p>{value}</p>
      </div>
    </div>
  );
}
