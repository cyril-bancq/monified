import React from "react";
import MovingIcon from "@mui/icons-material/Moving";
import "./CryptoCard.css";

export function CryptoCard({ crypto }) {
  return (
    <div className="crypto__container">
      <div className="crypto__left">
        <img src={crypto.image} alt={crypto.name} />
        <div className="crypto__left__text">
          <h3>{crypto.name}</h3>
          <p className="text">
            {crypto.price} <span>EUR</span>
          </p>
        </div>
      </div>
      <div className="crypto__right">
        <MovingIcon className={crypto.variation > 0 ? "green" : "red"} />
        <p className={crypto.variation > 0 ? "green" : "red"}>
          {crypto.variation}%
        </p>
      </div>
    </div>
  );
}
