import React from "react";
import { useAllCrypto } from "../../services/Crypto";
import { Input } from "../utils/Input/Input";
import "./Dashboard.css";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import SearchIcon from "@mui/icons-material/Search";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
import Bitcoin from "../../assets/Bitcoin.png";
import Ethereum from "../../assets/Ethereum.png";
import Binance from "../../assets/Binance.png";
import CurrencyExchangeIcon from "@mui/icons-material/CurrencyExchange";
import GroupsIcon from "@mui/icons-material/Groups";
import { CryptoCard } from "../utils/Card/Crypto/CryptoCard";
import { StatsCard } from "../utils/Card/Stats/StatsCard";
import { NewsCard } from "../utils/Card/News/NewsCard";
import CandleChart from "../Chart/Chart";

const Cryptos = () => [
  {
    name: "Bitcoin",
    price: 19169.83,
    variation: 0.5,
    image: Bitcoin,
  },
  {
    name: "Ethereum",
    price: 1405.2,
    variation: -2.5,
    image: Ethereum,
  },
  {
    name: "Binance",
    price: 267.31,
    variation: 1.6,
    image: Binance,
  },
];

export function Dashboard() {
  const [allCrypto, loading, error] = useAllCrypto();

  return (
    <div className="dashboard__container">
      <div className="dashboard__header">
        <div className="dashboard__header__left">
          <h1>Dashboard</h1>
          <p>With all of necessary tools in today’s market</p>
        </div>
        <div className="dashboard__header__middle">
          <Input
            placeholder="Select a date..."
            type="text"
            onChange={() => {}}
            icon={<CalendarMonthIcon fontSize="small" />}
          />
          <Input
            placeholder="Search..."
            onChange={() => {}}
            icon={<SearchIcon fontSize="small" />}
          />
        </div>
        <div className="dashboard__header__right">
          <img className="dashboard__circle" src={Bitcoin} />
          <KeyboardArrowLeftIcon
            sx={{ fontSize: 25 }}
            color="#acb5ca"
            style={{ cursor: "pointer" }}
          />
        </div>
      </div>
      <div className="dashboard__content">
        <div className="dashboard__content__top">
          {Cryptos().map((crypto, index) => (
            <CryptoCard key={index} crypto={crypto} />
          ))}
        </div>
        <div className="dashboard__content__middle">
          <div className="dashboard__content__middle__left">
            <CandleChart />
          </div>
          <div className="dashboard__content__middle__right">
            <div className="dashboard__content__middle__right__stats">
              <h1>Stats</h1>
              <div className="dashboard__stats__card">
                <StatsCard
                  icon={<CurrencyExchangeIcon sx={{ fontSize: 25 }} />}
                  title="Cryptos"
                  value="654"
                  backcolor={"rgba(171, 33, 236, 0.6)"}
                  color={"#AB21EC"}
                />
                <StatsCard
                  icon={<GroupsIcon sx={{ fontSize: 25 }} />}
                  title="Total Users"
                  value="456 765"
                  backcolor={"rgba(89, 228, 215, 0.6)"}
                  color={"#59E4D7"}
                />
              </div>
            </div>
            <NewsCard />
          </div>
        </div>
      </div>
    </div>
  );
}
