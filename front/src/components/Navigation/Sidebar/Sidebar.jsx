import React, { useState } from "react";
import "./Sidebar.css";
import DashboardIcon from "@mui/icons-material/Dashboard";
import AccountBalanceIcon from "@mui/icons-material/AccountBalance";
import FeedIcon from "@mui/icons-material/Feed";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";
import Logo from "../../../assets/logo.png"

export function Sidebar() {
  const [menu, setMenu] = useState([
    { name: "Dashboard", icon: <DashboardIcon /> },
    { name: "News", icon: <FeedIcon /> },
    { name: "Cryptos", icon: <AccountBalanceIcon /> },
    { name: "Account", icon: <ManageAccountsIcon /> },
  ]);

  return (
    <div className="sidebar__container">
      <div className="sidebar__header">
        <div className="sidebar__logo"><img src={Logo} alt="" /></div>
      </div>
      <div className="sidebar__menu">
        <ul>
          {menu.map((item, index) => (
            <div className="sidebar__list__menu" key={index}>
              <div className="sidebar__half__circle" />
              <li key={index}>
                {item.icon} {item.name}
              </li>
            </div>
          ))}
        </ul>
      </div>
    </div>
  );
}
