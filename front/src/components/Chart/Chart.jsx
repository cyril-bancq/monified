import React, { useState, useEffect } from "react";
import Chart from "react-apexcharts";
import { useBitcoin } from "../../services/Crypto";

export default function CandleChart() {
  const [bitcoin, loading, error] = useBitcoin();
  const [data, setData] = useState({
    options: {
      chart: {
        id: "crypto-candle",
      },
      grid: {
        show: false,
      },
      xaxis: {
        type: "category",
        tickPlacement: "none",
      },
      fill: {
        type: "gradient",
        gradient: {
          shadeIntensity: 1,
          opacityFrom: 0.7,
          opacityTo: 0.9,
          stops: [0, 100],
        },
      },
    },
    series: [],
  });
  return (
    <div className="row">
      <div className="mixed-chart">
        <Chart
          options={data.options}
          series={data.series}
          type="candlestick"
          width="1000"
        />
      </div>
    </div>
  );
}
