const API_USER = "http://localhost:8080/api/users";
const AUTH_LOGIN = "http://localhost:8080/api/auth/login";
const AUTH_REGISTER = "http://localhost:8080/api/auth/register";
const AUTH_MICROSOFT = "http://localhost:8080/api/auth/microsoft";
const AUTH_GOOGLE = "http://localhost:8080/api/auth/google";
const API_ROLE = "http://localhost:8080/api/roles";
const API_CRYPTO = "http://localhost:8080/api/cryptos";
const API_BOOKMARK = "http://localhost:8080/api/bookmarks";
const API_NEWS = "http://localhost:8080/api/news";
const API_PICTURE = "http://localhost:8080/api/pictures";

export default {
  API_USER,
  AUTH_LOGIN,
  AUTH_REGISTER,
  AUTH_MICROSOFT,
  AUTH_GOOGLE,
  API_ROLE,
  API_CRYPTO,
  API_BOOKMARK,
  API_NEWS,
  API_PICTURE,
};
