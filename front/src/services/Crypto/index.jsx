import axios from "axios";
import { useState, useEffect } from "react";
import URL from "../constant";

export const useAllCrypto = () => {
  const [allCrypto, setAllCrypto] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const getAllCrypto = async () => {
      try {
        const response = await axios.get(URL.API_CRYPTO);
        setAllCrypto(response.data.data);
      } catch (error) {
        setError(error);
      } finally {
        setLoading(false);
      }
    };
    getAllCrypto();
  }, []);

  return [allCrypto, loading, error];
};

export const useCrypto = (id) => {
  const [crypto, setCrypto] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const getCrypto = async () => {
      try {
        const response = await axios.get(`${URL.API_CRYPTO}/${id}`);
        setCrypto(response.data.data);
      } catch (error) {
        setError(error);
      } finally {
        setLoading(false);
      }
    };
    getCrypto();
  }, [id]);

  return [crypto, loading, error];
};

export const useBitcoin = () => {
  const [bitcoin, setBitcoin] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const getBitcoin = async () => {
      try {
        const response = await axios.get(`${URL.API_CRYPTO}/bitcoin`);
        setBitcoin(response.data.data);
      } catch (error) {
        setError(error);
      } finally {
        setLoading(false);
      }
    };
    getBitcoin();
  }, []);

  return [bitcoin, loading, error];
};
