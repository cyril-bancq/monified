const dotenv = require('dotenv');
const app = require('./app');

const DatabaseService = require("./src/services/Database/database.services");
const DBData = new DatabaseService();
const CryptoService = require("./src/services/Crypto/crypto.services");
const Crypto = new CryptoService();

const RssFieldService = require("./src/services/RssField/rssField.services");
const RssField = new RssFieldService();

const setUpExpress = () => {
  dotenv.config({ path: ".env" });

  const port = process.env.PORT || 8080;

  // SYNC DB
  const db = require("./src/models");

  db.sequelize.sync({force:true}).then(() => {
    console.log("Loading Sync DB...");
    DBData.createMandatoryRoles();
    DBData.createMandatoryUsers();
    // RssField.cryptoNews();
    Crypto.setAllCryptoDB();
    setInterval(() => {
      Crypto.setAllCryptoDB();
    }, 21600000);
    console.log("Sync DB loaded successfully.");
  });

  app.listen(port, () => {
    console.log(`Server is running on port ${port}.`);
  });

  // In case of an error
  app.on("error", (appErr, appCtx) => {
    console.error("app error", appErr.stack);
    console.error("on url", appCtx.req.url);
    console.error("with headers", appCtx.req.headers);
  });
};

setUpExpress();
