const UserService = require("../src/services/User/user.services");
const AuthService = require("../src/services/Auth/auth.services");
const DatabaseService = require("../src/services/Database/database.services");
const User = require("../src/models/index")["User"];
const db = require("../src/models/index")["sequelize"];

let dbConnection;

describe("UserService", () => {
  beforeAll(async () => {
    const DBData = new DatabaseService();
    await db.sync({ force: true }).then((connection) => {
      dbConnection = connection;
      DBData.createMandatoryRoles();
      DBData.createMandatoryUsers();
    });
  });

  it("should create a user", async () => {
    const authService = new AuthService();
    const newUser = {
      email: "test@example.com",
      password: "password",
      firstname: "Test",
      lastname: "User",
      username: "testuser",
    };
    const user = await authService.register(newUser);
    expect(user.email).toBe("test@example.com");
    expect(user.firstname).toBe("Test");
    expect(user.lastname).toBe("User");
    expect(user.username).toBe("testuser");
  });

  it("should get a user", async () => {
    const userService = new UserService();
    const user = await userService.findOne(1);
    expect(user).toHaveProperty("id");
  });

  it("should get all users", async () => {
    const userService = new UserService();
    const users = await userService.findAll();
    expect(users).not.toBeNull();
  });

  it("should delete a user", async () => {
    const userService = new UserService();
    const user = await userService.delete(1);
    expect(user).not.toBeNull();
  });

  it("should get a user by email", async () => {
    const userService = new UserService();
    const user = await userService.findByEmail("issam.hadjal@epitech.eu");
    expect(user).toHaveProperty("id");
    expect(user.email).toBe("issam.hadjal@epitech.eu");
  });

  it("should get a user by username", async () => {
    const userService = new UserService();
    const user = await userService.findByUsername("issam.hadjal");
    expect(user).toHaveProperty("id");
    expect(user.username).toBe("issam.hadjal");
  });

  it("should check is User Email exits", async () => {
    const userService = new UserService();
    const user = await userService.checkUserEmail("issam.hadjal@epitech.eu");
    expect(user).toBe(true);
  });

  it("should check is User Username exits", async () => {
    const userService = new UserService();
    const user = await userService.checkUserUsername("issam.hadjal");
    expect(user).toBe(true);
  });

  it("should check is User is Admin", async () => {
    const userService = new UserService();
    const user = await userService.checkUserisAdmin(5);
    expect(user).toBe(true);
  });

  it("should update a user", async () => {
    const userService = new UserService();
    const user = await userService.update(4, {
      email: "example@gmail.com",
      firstname: "Example",
      lastname: "User",
      username: "exampleuser",
    });
    expect(user).not.toBeNull();
  });
});
