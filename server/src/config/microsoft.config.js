require("dotenv").config();




module.exports = {
  clientID: process.env.MICROSOFT_CLIENT_ID,
  clientSecret: process.env.MICROSOFT_CLIENT_SECRET,
  callbackURL: process.env.MICROSOFT_CALLBACK_URI,
  tenantID: process.env.MICROSOFT_TENANT_ID,
  scope: ["user.read"],
};
