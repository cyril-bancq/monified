require("dotenv").config();

module.exports = {
  clientID: process.env.GOOGLE_CLIENT_ID,
  clientSecret: process.env.GOOGLE_CLIENT_SECRET,
  callbackURL: process.env.GOOGLE_CALLBACK_URI,
  scope: ["profile"],
  password: process.env.TEMP_PASSWORD,
};
