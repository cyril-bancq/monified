const Crypto = require("../../models/index")["Crypto"];
const Pic = require("../../models/index")["Picture"];
const User = require("../../models/index")["User"];
const qs = require("qs");
const Picture = require("../../services/Picture/picture.services");

const dotenv = require("dotenv");
const axios = require("axios");
const moment = require("moment");
dotenv.config();

const { Spot } = require("@binance/connector");
const apiKeys = process.env.BINANCE_API_KEY;
const apiSecret = process.env.BINANCE_SECRET_KEY;
const binance = new Spot(apiKeys, apiSecret);

const { filterCryptoData } = require("../../helpers/index");

class CryptoService {
  constructor() {
    this.crypto = Crypto;
  }

  /**
   * @param {Crypto} crypto
   * @returns {Promise<Crypto>}
   * @description Create crypto in db
   */
  async create(crypto) {
    return await this.crypto.create(crypto);
  }

  /**
   * @returns {Promise<Crypto[]>}
   * @description Return all cryptos
   */
  async findAll() {
    return await this.crypto
      .findAll({
        include: [
          {
            model: User,
            as: "users",
          },
          {
            model: Pic,
            as: "picture"
          }
        ],
        order: [["historic", "DESC"]],
      })
      .then((cryptos) => {
        if (cryptos.length !== 0) {
          return cryptos;
        } else {
          throw new Error("No cryptos found");
        }
      });
  }

  /**
   * @param {Number} id
   * @returns {Promise<Crypto>}
   */
  async findOne(id) {
    return await this.crypto
      .findOne({
        where: {
          id: id,
        },
        include: [
          {
            model: User,
            as: "users",
          },
          {
            model: Pic,
            as: "picture"
          },
        ],
      })
      .then((crypto) => {
        if (crypto) {
          return crypto;
        }
      })
      .catch((error) => {
        throw new Error(error.message);
      });
  }

  /**
   * @returns {Promise<Crypto>}
   * @description Find Bitcoin Crypto
   */
  async findBitcoin() {
    return await this.findBySymbol("BTCUSDT")
      .then((crypto) => {
        return crypto;
      })
      .catch(() => {
        throw new Error("Bitcoin not available");
      });
  }

  /**
   * @param {Number} id
   * @param {Crypto} crypto
   * @returns {Promise<Crypto>}
   */
  async update(id, crypto) {
    return await this.crypto
      .update(crypto, {
        where: {
          id: id,
        },
      })
      .then((crypto) => {
        if (crypto) {
          return crypto;
        }
      })
      .catch((error) => {
        throw new Error(error.message);
      });
  }

  /**
   * @param {Number} id
   * @returns {Promise<Crypto>}
   */
  async delete(id) {
    return await this.crypto
      .destroy({
        where: {
          id: id,
        },
      })
      .then((num) => {
        if (num == 1) {
          return this.findAll();
        }
      })
      .catch(() => {
        throw new Error("Crypto not found with id " + id);
      });
  }

  /**
   * @param {String} symbol
   * @returns {Promise<Crypto>}
   */
  async findBySymbol(symbol) {
    return await this.crypto
      .findOne({
        where: {
          symbol: symbol,
        },
      })
      .then((crypto) => {
        return crypto;
      })
      .catch(() => {
        throw new Error("Crypto not found with symbol " + symbol);
      });
  }

  /**
   * @param {String} symbol
   * @returns {Promise<Crypto>}
   * @description Get price of a crypto
   */
  async getPriceOfACrypto(symbol) {
    const crypto = await this.findBySymbol(symbol);
    return {
      symbol: crypto.symbol,
      price: crypto.price,
    };
  }

  /**
   * @param {String} symbol
   * @returns {Promise<Crypto>}
   * @description Checks if a crypto with the given symbol already exists
   */
  async cryptoExists(symbol) {
    const crypto = await this.findBySymbol(symbol);
    return crypto ? true : false;
  }

  /**
   * @returns {Promise<Crypto[]>}
   * @description Get all cryptos from Binance format USDT and create them in the database
   */
  async setAllCryptoDB() {
    const url = "https://www.binance.com/api/v3/ticker/price";
    return await axios
      .get(url)
      .then((res) => {
        const filteredData = filterCryptoData(res.data);
        filteredData.forEach(async (data) => {
          let newUrl = `https://api.binance.com/api/v3/ticker/24hr?symbol=${data.symbol}`;
          await axios
            .get(newUrl)
            .then((res) => {
              let symbol = data.symbol;
              let price = parseFloat(res.data.lastPrice).toFixed(2);
              let volume = parseFloat(res.data.volume).toFixed(2);
              let variation = parseFloat(res.data.priceChangePercent).toFixed(
                2
              );
              if (parseFloat(res.data.lastPrice) <= 50) {
                if (parseFloat(res.data.lastPrice) <= 10) return;
                const crypto = {
                  symbol: symbol,
                  price: price,
                  volume: volume,
                  variation: variation,
                  historic: {},
                };
                this.create(crypto);
              } else {
                const crypto = {
                  symbol: symbol,
                  price: price,
                  volume: volume,
                  variation: variation,
                  historic: {},
                };
                this.getHistoricOfCrypto(crypto).then((res) => {
                  this.create(res).then((res) => {
                    let symb = data.symbol.slice(0, -4);
                    Picture.setPictureForAllCryptos(symb, res.dataValues.id)
                  });
                });
              }
            })
            .catch((error) => {
              throw new Error(error.message);
            });
        });
      })
      .catch((error) => {
        throw new Error(error.message);
      });
  }

  /**
   * @param {String} symbol
   * @param {String[]} options
   * @returns {Promise<Crypto[]>}
   * @description Return candlestick data for a crypto
   */
  async getHistoricOfCrypto(crypto) {
    if (!crypto) {
      throw new Error("Crypto is required");
    }

    // REQUEST
    const baseUrl = `https://data.binance.com`;
    const endpoint = `/api/v3/klines`;
    const headers = {
      BINANCE_API_KEY: apiKeys,
      BINANCE_SECRET_KEY: apiSecret,
    };
    // SET ALL THE TIME
    const currentDate = moment().unix() * 1000;
    const oneHourAgo = moment().subtract(1, "hours").unix() * 1000;
    const fourHourAgo = moment().subtract(4, "hours").unix() * 1000;
    const sixHourAgo = moment().subtract(6, "hours").unix() * 1000;
    const eightHourAgo = moment().subtract(8, "hours").unix() * 1000;
    const twelveHourAgo = moment().subtract(12, "hours").unix() * 1000;
    const oneDayAgo = moment().subtract(1, "days").unix() * 1000;
    const threeDayAgo = moment().subtract(3, "days").unix() * 1000;
    const oneWeekAgo = moment().subtract(1, "weeks").unix() * 1000;
    const oneMonthAgo = moment().subtract(1, "months").unix() * 1000;

    const intervals = {
      hour: "1h",
      // fourHour: "4h",
      // sixHour: "6h",
      // eightHour: "8h",
      // twelveHour: "12h",
      // day: "1d",
      // threeDay: "3d",
      // week: "1w",
      // oneMonth: "1M",
    };

    const time = {
      // "1h": {
      //   start: oneHourAgo,
      //   end: currentDate,
      // },
      fourHour: {
        start: fourHourAgo,
        end: currentDate,
      },
      // "6h": {
      //   start: sixHourAgo,
      //   end: currentDate,
      // },
      // "8h": {
      //   start: eightHourAgo,
      //   end: currentDate,
      // },
      // "12h": {
      //   start: twelveHourAgo,
      //   end: currentDate,
      // },
      day: {
        start: oneDayAgo,
        end: currentDate,
      },
      // "3d": {
      //   start: threeDayAgo,
      //   end: currentDate,
      // },
      week: {
        start: oneWeekAgo,
        end: currentDate,
      },
      oneMonth: {
        start: oneMonthAgo,
        end: currentDate,
      },
    };

    let historic = {};
    for (let interval in intervals) {
      if (!intervals[interval]) {
        throw new Error(`Interval ${interval} is required`);
      }
      for (let timePeriod in time) {
        const { start, end } = time[timePeriod];
        const val = intervals[interval];
        const symbol = crypto.symbol;
        const queryParams = {
          symbol: `${symbol}`,
          interval: val,
          startTime: start,
          endTime: end,
          limit: 365,
        };
        const url = baseUrl + endpoint + "?" + qs.stringify(queryParams);
        await axios
          .get(url, { headers })
          .then((res) => {
            for (let bar of res.data) {
              if (!historic[timePeriod]) {
                historic[timePeriod] = [];
              }
              historic[timePeriod].push({
                Symbol: crypto.symbol,
                Open: bar[1],
                Close: bar[4],
                High: bar[2],
                Low: bar[3],
                Volume: bar[5],
                Time: moment(bar[0]).format("YYYY-MM-DD HH:mm:ss"),
              });
            }
          })
          .catch((error) => {
            throw new Error(error.message);
          });
      }
    }

    Object.assign(crypto.historic, historic);
    return crypto;
  }
}

module.exports = CryptoService;
