const passport = require('passport');
const MicrosoftStrategy = require("passport-microsoft").Strategy;
const config = require('../../config/microsoft.config');

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});

passport.use(
  new MicrosoftStrategy(
    {
      clientID: config.clientID,
      clientSecret: config.clientSecret,
      callbackURL: config.callbackURL,
      tenantID: config.tenantID,
      scope: config.scope,
      tokenURL: "https://login.microsoftonline.com/common/oauth2/v2.0/token",
      authorizationURL:
        "https://login.microsoftonline.com/common/oauth2/v2.0/authorize",
    },
    (accessToken, refreshToken, params, profile, done) => {
      return done(null, profile);
    }
  )
);
