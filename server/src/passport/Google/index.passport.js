const passport = require("passport");
const config = require("../../config/google.config");
const authConfig = require("../../config/auth.config");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const userService = require("../../services/User/user.services");
const UserService = new userService();
const authService = require("../../services/Auth/auth.services");
const AuthService = new authService();
const jwt = require("jsonwebtoken");

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});

passport.use(
  new GoogleStrategy(
    {
      clientID: config.clientID,
      clientSecret: config.clientSecret,
      callbackURL: config.callbackURL,
      scope: config.scope,
    },
    async (accessToken, refreshToken, profile, done) => {
      try {
        const email = profile.emails[0].value;
        const userFound = await UserService.findByEmail(email).then(
          async (user) => {
            if (user) {
              const token = jwt.sign({ id: user.id }, authConfig.secret, {
                algorithm: "HS256",
                expiresIn: 86400, // 24 hours
              });
              return token;
            }
            const newUser = await AuthService.register({
              email: email,
              username: profile.name.givenName + "." + profile.name.familyName + "." + profile.id,
              firstname: profile.name.givenName,
              lastname: profile.name.familyName,
              password: config.password,
              role_id: 1,
            });
            const token = jwt.sign({ id: newUser.id }, authConfig.secret, {
              algorithm: "HS256",
              expiresIn: 86400, // 24 hours
            });
            return token;
          }
        );
        return done(null, userFound);
      } catch (error) {
        console.log(error);
      }
    }
  )
);
